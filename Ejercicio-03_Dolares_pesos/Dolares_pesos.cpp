/**************************************************************
	Name: Dolares a pesos
	Copyright: GG CDMX, 2020
	Author: Gerardo Gonz�lez Guerrero
	Date: 14/08/2020 20:12
	Description: Programa que recibe un monto en d�lares (USD) 
	y devuelva la cantidad equivalente en pesos mexicanos (MXN)
***************************************************************/
#include <stdio.h>

int main (){
	
	float USD = 0;
	float MXN = 21.97; //Intercambio al dia de 14 de Agosto de 2020
	
	printf ("Ingrese la cantidad de dolares a convertir: ");
	scanf ("%f", &USD);
	USD = USD * MXN;
	printf ("La cantidad obtenida en pesos mexicanos es: %.2lf", USD);
	
	getchar ();
	return 0;
}
