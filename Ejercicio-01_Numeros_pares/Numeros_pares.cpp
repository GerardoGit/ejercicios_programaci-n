/***********************************************************
	Name: Numeros_pares
	Copyright: GG CDMX, 2020
	Author: Gerardo Gonz�lez Guerrero
	Date: 13/08/2020 19:19
	Description: Programa que imprime todos los numeros pares
	del 0 al 100
***********************************************************/
#include <stdio.h>

int main(){
    int contador = 0;

    while (contador < 100){
        contador = contador + 2;
        printf ("%d\n", contador);
    }
    
    getchar ();
    return 0;
}
