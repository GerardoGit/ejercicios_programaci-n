/**************************************************************
	Name: Perimetro triangulo equilatero
	Copyright: GG CDMX, 2020
	Author: Gerardo Gonz�lez Guerrero
	Date: 14/08/2020 20:54
	Description: Programa que calcula el per�metro de un 
	tri�ngulo equil�tero. 
***************************************************************/
#include <stdio.h>

int main (){
	
	float lado = 0; 
	
	printf ("Programa para calcular el perimetro de un triangulo equilatero\n\n");
	printf ("Ingrese el valor de cualquier lado del triangulo: ");
	scanf ("%f", &lado);
	lado = 3 * lado;
	printf ("El perimetro de tu triangulo equilatero es: %.2lf", lado);
	
	getchar();
	return 0;
}
