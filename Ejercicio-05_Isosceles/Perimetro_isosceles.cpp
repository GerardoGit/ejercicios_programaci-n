/**************************************************************
	Name: Perimetro triangulo isosceles
	Copyright: GG CDMX, 2020
	Author: Gerardo González Guerrero
	Date: 14/08/2020 21:22
	Description: Programa que calcula el perímetro de un 
	triángulo isosceles. 
***************************************************************/
#include <stdio.h>

int main (){
	
	float lado = 0, base = 0, perimetro = 0;
	
	printf ("Programa para calcular el perimetro de un triangulo isosceles\n\n");
	printf ("Ingrese el valor de la base: ");
	scanf ("%f", &base);
	printf ("Ingrese el valor de cualquier lado del triangulo: ");
	scanf ("%f", &lado);
	perimetro = ((2 * lado) + base);
	printf ("El perimetro de tu triangulo isosceles es: %.2lf", perimetro);
	
	getchar();
	return 0;
}
