/**************************************************************
	Name: Perimetro triangulo escaleno
	Copyright: GG CDMX, 2020
	Author: Gerardo González Guerrero
	Date: 14/08/2020 21:40
	Description: Programa que calcula el perímetro de un 
	triángulo escaleno. 
***************************************************************/
#include <stdio.h>

int main (){
	
	float ladoA = 0, ladoB = 0, ladoC = 0, perimetro= 0;
	
	printf ("Programa para calcular el perimetro de un triangulo escaleno\n\n");	
	printf ("Ingrese el valor del lado A: ");
	scanf ("%f", &ladoA);
	printf ("Ingrese el valor del lado B: ");
	scanf ("%f", &ladoB);
	printf ("Ingrese el valor del lado C: ");
	scanf ("%f", &ladoC);
	perimetro = (ladoA + ladoB + ladoC);
	printf ("El perimetro de tu triangulo escaleno es: %.2lf", perimetro);
	
	getchar();
	return 0;
}
