/*************************************************************************
	Name: Suma de consecutivos
	Copyright: GG CDMX, 2020
	Author: Gerardo Gonz�lez Guerrero
	Date: 14/08/2020 17:34
	Description: Programa que recibe un n�mero entre 1 y 50, y devuelve 
	la suma de los n�meros consecutivos del 1 hasta el n�mero recibido.
************************************************************************/
#include <stdio.h>

int numero = 0;

/* Funcion suma: Permite realizar la suma consecutiva desde 
el numero 1 hasta el numero recibido por el usuario */
int suma (){	
	numero = (numero * (numero +1)/2);
	printf ("%d", numero);
}

int main(){
	printf ("Ingrese un numero del 1 al 50:  ");
	scanf ("%d", &numero);
	if (numero > 0 && numero <= 50){
		printf("Numero valido\n");
		printf ("La suma consecutiva hasta tu numero es: ");
		suma();
	} else {
		printf ("Numero no valido");
	}
getchar ();
return 0;
}
